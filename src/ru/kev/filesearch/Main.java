package ru.kev.filesearch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


/**
* Класс для запуска программы.
 * Указывается путь к файлу reader , куда будет записываться результат работы.
 * С помощью клавиатуры вводится текст который нужно найти в файлах.
 *
* @author Karnauhov Evgeniy , group 15IT18
*/
public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        try (BufferedReader reader = new BufferedReader(new FileReader("src\\ru\\kev\\filesearch\\reader.txt"))) {
            String string = reader.readLine();
            System.out.print("Введите текст для поиска:");
            String message = scan.next();

            Search.main(new File(string),message);
        } catch (NullPointerException e) {

        }
    }
}
