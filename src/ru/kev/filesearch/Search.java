package ru.kev.filesearch;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс , в котором выполнена реализация поиска текста в файлах.
 *
 * @author Karnauhov Evgeniy , group 15IT18
 */
class Search extends Thread {

    private File file;

    private String message;

    private static volatile BufferedWriter result;

    Search(File file, String message) {
        this.file = file;
        this.message = message;
    }

 /**
  * Происходит запись основного сообщения
  */

    private synchronized void writeFoundedMessage(String str) {
        try {
            result.write(str);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Метод задаёт условия поиска.
     *
     * @param pathDirectory - каталог поиска
     * @param messageForSearch - сообщение для поиска
     */

    private static void fileLook(File pathDirectory, String messageForSearch) {
        try {
            File[] files = pathDirectory.listFiles();
            assert files != null;
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile() && files[i].getName().matches(".*\\.txt$")) {
                    Search thread = new Search(files[i], messageForSearch);
                    thread.start();
                    waitThreadDie(thread);
                } else if (files[i].isDirectory()) {
                    fileLook(files[i], messageForSearch);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        String string;
        Pattern searchStr = Pattern.compile(message);
        Matcher mSearchStr;

        try (BufferedReader inFile = new BufferedReader(new FileReader(file))) {
            for (int i = 1; (string = inFile.readLine()) != null; i++) {

                mSearchStr = searchStr.matcher(string);

                if (mSearchStr.find()) {
                    writeFoundedMessage("Path: " + file.getPath() + "; Message for search: " + mSearchStr.group() +
                            "; Line : " + i + "\r\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод указывающий путь к файлу , куда будет записываться результаты поиска.
     *
     * @param path - путь.
     * @param message - сообщение.
     * @throws IOException исключения.
     */
    public static void main(File path, String message) throws IOException {

        try {

            result = new BufferedWriter(new FileWriter("src\\ru\\kev\\filesearch\\result.txt"));

            fileLook(path, message);

            result.close();

        } catch (NullPointerException e) {

        }

    }

    /**
     * Метод ожидающий поток его оканчания.
     *
     * @param thread - поток.
     */
    private static void waitThreadDie(Thread thread) {
        try {
            if (thread.isAlive()) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}